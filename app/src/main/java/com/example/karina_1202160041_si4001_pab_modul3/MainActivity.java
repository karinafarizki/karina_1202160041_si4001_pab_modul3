package com.example.karina_1202160041_si4001_pab_modul3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private RecyclerView RecyclerView;
    private ArrayList<User> daftarUser;
    private AdapterView adapterUser;
    ArrayAdapter<String> adapter;

    final Context context = this;
    private TextView nama, pekerjaan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView = findViewById(R.id.idRecyclerView);

        int column = getResources().getInteger(R.integer.column);
        RecyclerView.setLayoutManager(new GridLayoutManager(this, column));

        daftarUser = new ArrayList<>();
        if (savedInstanceState != null) {
            daftarUser.clear();
            for (int i = 0; i < savedInstanceState.getStringArrayList("nama").size() ; i++) {
                daftarUser.add(new User(savedInstanceState.getStringArrayList("nama").get(i),
                        savedInstanceState.getStringArrayList("pekerjaan").get(i),
                        savedInstanceState.getIntegerArrayList("gender").get(i)));
            }
        } else{
            reload();
        }


        adapterUser = new AdapterView(daftarUser, this);
        RecyclerView.setAdapter(adapterUser);

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();
                Collections.swap(daftarUser, from ,to);
                adapterUser.notifyItemMoved(from,to);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                daftarUser.remove(viewHolder.getAdapterPosition());
                adapterUser.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        helper.attachToRecyclerView(RecyclerView);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater li = LayoutInflater.from(context);
                View prompts = li.inflate(R.layout.prompts, null);

                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                alertDialogBuilder.setView(prompts);

                final EditText inNama = (EditText) prompts.findViewById(R.id.inputNama);
                final EditText inPekerjaan = (EditText) prompts.findViewById(R.id.inputPekerjaan);
                final Spinner inGender = (Spinner) prompts.findViewById(R.id.chGender);

                String[] gender = {"Male", "Female"};

                ArrayAdapter<String>adapter = new ArrayAdapter(alertDialogBuilder.getContext(), android.R.layout.simple_spinner_item, gender);
                inGender.setAdapter(adapter);


                        alertDialogBuilder.setCancelable(false).setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                daftarUser.add(new User(inNama.getText().toString(),inPekerjaan.getText().toString(),inGender.getSelectedItemPosition()+1));
                                adapterUser.notifyDataSetChanged();
                                Toast.makeText(MainActivity.this, "Berhasil Menambahkan User", Toast.LENGTH_LONG).show();
                                dialogInterface.cancel();
                            }
                        })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

        });

    }

    void reload(){
        daftarUser.clear();
        daftarUser.add(new User("Tayo", "Data Analyst", 1));
        daftarUser.add(new User("Lani", "Data Science", 2));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ArrayList<String>tempListNama = new ArrayList<>();
        ArrayList<String>tempListPekerjaan = new ArrayList<>();
        ArrayList<Integer>tempListGender = new ArrayList<>();
        for (int i = 0; i <daftarUser.size() ; i++) {
            tempListNama.add(daftarUser.get(i).getNama());
            tempListPekerjaan.add(daftarUser.get(i).getPekerjaan());
            tempListGender.add(daftarUser.get(i).getProfile());
        }

        outState.putStringArrayList("nama",tempListNama);
        outState.putStringArrayList("pekerjaan",tempListPekerjaan);
        outState.putIntegerArrayList("gender",tempListGender);
        super.onSaveInstanceState(outState);

    }

}
