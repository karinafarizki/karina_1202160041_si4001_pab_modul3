package com.example.karina_1202160041_si4001_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterView extends RecyclerView.Adapter<AdapterView.ViewHolder> {

    private ArrayList<User> listUser;
    private Context mContext;

    public AdapterView(ArrayList<User> listUser, Context mContext) {
        this.listUser = listUser;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.user_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User curUser = listUser.get(position);
        holder.bindTo(curUser);

    }

    @Override
    public int getItemCount() {
        return listUser.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        private TextView nama, pekerjaan;
        private ImageView profile;
        private int profileNumber;

        public ViewHolder(View view) {
            super(view);

            nama = view.findViewById(R.id.list_nama);
            pekerjaan = view.findViewById(R.id.list_pekerjaan);
            profile = view.findViewById(R.id.profile);

            //Bisa pake implements bisa gini aja
            itemView.setOnClickListener(this);

        }

        public void bindTo(User curUser) {
            nama.setText(curUser.getNama());
            pekerjaan.setText(curUser.getPekerjaan());
            profileNumber = curUser.getProfile();

            profileNumber = curUser.getProfile();
            switch (curUser.getProfile()) {
                case 1:
                    profile.setImageResource(R.drawable.boy);
                    break;
                case 2:
                default:
                    profile.setImageResource(R.drawable.girl);
                    break;

            }
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(),DetailUser.class);
            intent.putExtra("nama",nama.getText().toString());
            intent.putExtra("gender",profileNumber);
            intent.putExtra("pekerjaan",pekerjaan.getText().toString());
            view.getContext().startActivity(intent);
        }
    }
}
