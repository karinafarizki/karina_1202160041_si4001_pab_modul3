package com.example.karina_1202160041_si4001_pab_modul3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailUser extends AppCompatActivity {

    private TextView txtNama, txtPekerjaan;
    private ImageView imgProfile;
    private int avatarCode;
    private String Nama,Pekerjaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_user);


        txtNama = findViewById(R.id.txtNama);
        txtPekerjaan = findViewById(R.id.txtPekerjaan);
        imgProfile = findViewById(R.id.imgProfile);

        Nama = getIntent().getStringExtra("nama");
        Pekerjaan = getIntent().getStringExtra("pekerjaan");
        avatarCode = getIntent().getIntExtra("gender",2);

        txtNama.setText(Nama);
        txtPekerjaan.setText(Pekerjaan);
        switch (avatarCode){
            case 1 :
                imgProfile.setImageResource(R.drawable.boy);
                break;
            case 2 :
            default:
                imgProfile.setImageResource(R.drawable.girl);
                break;
        }
    }

}
