package com.example.karina_1202160041_si4001_pab_modul3;

public class User {

    private String nama;
    private String pekerjaan;
    private final int profile;


    public User(String nama, String pekerjaan, int profile) {
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.profile = profile;
    }

    public String getNama() {
        return nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public int getProfile() {
        return profile;
    }

}
